<?php
    // API group
    $app->group('/cron', function () use ($app, $config) {
        // Download rates daily
        $app->get('/download/:key', function ($key) use ($app, $config) {
            // prevent unauthorized use
            if ($key != $config->server_key) {
                $app->halt(403, 'Forbidden!');
            }

            // get rates and store them as CSV
            file_put_contents(
                'rates.csv',
                fopen('https://www.centralbank.go.ke/uploads/fx_rates/historical_data.csv', 'r')
            );
        });

        // process rates from rates.csv
        $app->get('/process/:key', function ($key) use ($app, $config) {
            // prevent unauthorized use
            if ($key != $config->server_key) {
                $app->halt(403, 'Forbidden!');
            }

            // read rates from rates.csv file already downloaded
            $csv = array_map('str_getcsv', file('rates.csv'));

            foreach ($csv as $rt) {
                // Find currency id based on alias
                $currency = R::findOne('currency', 'alias = :alias', [':alias' => $rt['1']]);
                $date = date('Y-m-d', strtotime($rt['0']));

                // Do not save images with date greater than today
                if (new DateTime() > new DateTime($date)) {
                    if ($currency != null && !check_exists_data($date, $currency->id)) { // ensure currency exists and the data for that date doesnt exist
                        // Populate rates database
                        $rate = R::dispense('rate');
                        $rate->currency_id = $currency->id; // Get currency id
                        $rate->mean = isset($rt['2']) ? $rt['2'] : null;
                        $rate->buying = isset($rt['3']) ? $rt['3'] : null;
                        $rate->selling = isset($rt['4']) ? $rt['4'] : null;
                        $rate->date = $date;
                        R::store($rate);
                    }
                }
            }
        });
    });
