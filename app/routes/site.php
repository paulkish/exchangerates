<?php

    $app->get('/', 'Site', function () use ($app) {
        $app->render('pages/site.php');
    })->name('site');

    $app->post('/register', function () use ($app, $config) {
        $email = $app->request()->post('email');
        if (validate_email($email)) { // Check if valid email
            if (!check_exist_email($email)) { // Check if email exists in the system
                $user = R::dispense('user');
                $user->email = $email;
                $user->authorized = 0; // Account status not activated
                $user->confirm_code = random_text($type = 'alnum', $length = 20); // Generate random code
                $user->create_date = date('Y-m-d H:i:s');
                R::store($user);

                $req = $app->request;
                $url = $config->baseUrl.$req->getRootUri().'/confirm/'.$user->confirm_code;

                // Create email to send to user to confirm address
                $subject = 'Confirm Email Address';
                $msg = '<p>Hello,</p>';
                $msg .= '<p>Thanks for registering on exchangerates.co.ke for our JSON API exchange rates service.</p>';
                $msg .= "<p>To receive an API key please verify your email address by <a href='$url'>clicking here</a><p>";
                $msg .= '<p>Regards,</p>';
                $msg .= "<p><a href='http://exchangerates.co.ke'>exchangerates.co.ke</a></p>";

                $send = email($email, $subject, $msg);
                if ($send) {
                    $app->flash('success', 'Please check your email for further instruction');
                    $app->redirect($app->urlFor('signup')); //redirect to /site
                } else {
                    $app->flash('error', 'Unable to send email');
                    $app->redirect($app->urlFor('signup')); //redirect to /site
                }
            } else {
                $app->flash('error', 'The email address is already in use');
                $app->redirect($app->urlFor('signup')); //redirect to /site
            }
        } else {
            $app->flash('error', 'Invalid email address');
            $app->redirect($app->urlFor('signup')); //redirect to /site
        }
    });

    $app->get('/confirm/:confirm', function ($confirm) use ($app) {
        // Check if confirm code is same as what is in database
        $user = R::findOne('user', 'confirm_code =:code AND authorized =:auth', [':code' => $confirm, ':auth' => 0]);
        if ($user != null) {
            // Create new key
            $key = $key = R::dispense('key');
            $key->api_key = random_text($type = 'alnum', $length = 20);
            $key->level = 1;
            $key->ignore_limits = 0;
            $key->create_date = date('Y-m-d H:i:s');
            R::store($key);

            // Authenticate user
            $user->key_id = $key->id;
            $user->authorized = 1;
            R::store($user);

            $new_key = $key->api_key;

            // Send Email to user with key
            $subject = 'exchangerates.co.ke API Key';
            $msg = '<p>Hello,</p>';
            $msg .= "<p>Your account is now active. Your API key is <b>$new_key</b><p>";
            $msg .= '<p>Regards,</p>';
            $msg .= "<p><a href='http://exchangerates.co.ke'>exchangerates.co.ke</a></p>";

            if (email($user->email, $subject, $msg)) {
                $app->flash('success', 'Your email address has been verfied. Your API Key has been sent to your email address');
                $app->redirect($app->urlFor('signup')); //redirect to /site
            } else {
                $app->flash('error', 'Unable to send email');
                $app->redirect($app->urlFor('signup')); //redirect to /site
            }
        } else {
            $app->flash('error', 'Invalid confirmation code');
            $app->redirect($app->urlFor('signup')); //redirect to /site
        }
    });

    $app->get('/about', 'Site', function () use ($app) {
        $uptime = uptime();
        $app->render('pages/about.php', array('uptime' => $uptime));
    });

    $app->get('/usage', 'Site', function () use ($app) {
        $app->render('pages/usage.php');
    });

    $app->get('/signup', 'Site', function () use ($app) {
        $app->render('pages/signup.php');
    })->name('signup');

    $app->get('/contact', 'Site', function () use ($app) {
        $app->render('pages/contact.php');
    });
