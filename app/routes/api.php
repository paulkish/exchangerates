<?php
    // API group
    $app->group('/api', 'APIrequest', function () use ($app) {
        $app->get('/', function () use ($app) {
            $app->response->redirect('latest', 303); //redirect to /api/latest
        });

        // Latest rates
        $app->get('/latest/key/:key', function ($key) use ($app) {
            // Check if API key is valid
            authenticate($key);

            // Return latest rates data
            $latest = R::findOne('rate', 'ORDER BY date DESC');

            // Return latest data based on date
            $rates = get_rates($latest->date);

            // Log request to db
            log2db(1, $key);

            $app->render(200, array('msg' => $rates)); // Render
        })->conditions(array('key' => '[a-zA-Z0-9]{20}'));

        // Rates for a specific date
        $app->get('/:date/key/:key', function ($date, $key) use ($app) {
            if (validateDate($date)) {
                // Check if API key is valid
                authenticate($key);

                // Return latest data based on date
                $rates = get_rates($date);

                // Log request to db
                log2db(1, $key);

                if ($rates != null) {
                    $app->render(200, array('msg' => $rates));
                } else {
                    $app->render(404, array('error' => true, 'msg' => 'No exchange rate data exists for that date'));
                }
            } else {
                throw new Exception('Incorrect date format! Correct format is YYYY-MM-DD');
            }
        })->conditions(array('date' => '[0-9]{4}-[0-9]{2}-[0-9]{2}', 'key' => '[a-zA-Z0-9]{20}'));

        // Rates for a specific time period
        $app->get('/:start/:end/key/:key', function ($start, $end, $key) use ($app) {
            if (validateDate($start) && validateDate($end)) {
                // Check if API key is valid
                authenticate($key);

                // Return latest data based on date
                $rates = get_rates_between($start, $end);

                // Log request to db
                log2db(1, $key);

                if ($rates != null) {
                    $app->render(200, array('msg' => $rates));
                } else {
                    $app->render(404, array('error' => true, 'msg' => 'No exchange rate data exists for that time period'));
                }
            } else {
                throw new Exception('Incorrect date format! Correct format is YYYY-MM-DD');
            }
        })->conditions(array('start' => '[0-9]{4}-[0-9]{2}-[0-9]{2}', 'end' => '[0-9]{4}-[0-9]{2}-[0-9]{2}', 'key' => '[a-zA-Z0-9]{20}'));
    });
