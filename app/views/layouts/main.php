<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Free JSON API for foreign exchange rates dating back to 2005, sourced from the Central Bank of Kenya">
		<meta name="author" content="Ngumii">
		<link rel="icon" href="favicon.ico">

		<title>exchangerates.co.ke</title>

		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
		<!-- Custom styles for this template -->
		<link href="css/main.css" rel="stylesheet">

		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="icon" href="favicon.ico" type="image/x-icon">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	</head>
	<body>
		<div class="container">
			<!-- header -->
			<div class="header">
				<nav class="navbar navbar-default">
					<div class="navbar-header">
					    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					    </button>
					    <a class="navbar-brand" href="#"><span class="text-muted brand-header">exchangerates.co.ke</span></a>
				    </div>
				    <div class="collapse navbar-collapse" id="navbar-collapse">
						<ul class="nav navbar-nav pull-right">
							<li role="presentation"><a href="/">Home</a></li>
							<li role="presentation"><a href="about">About</a></li>
							<li role="presentation"><a href="usage">Usage</a></li>
							<li role="presentation"><a href="contact">Contact</a></li>
						</ul>
					</div>
				</nav>
			</div> <!-- /header -->

			<div class="bottom-border"></div>

			<div class="container-fluid">
				<?php echo $yield ?>
			</div>
			
			<div class="bottom-border"></div>

			<footer class="footer">
				<center><p>&copy; Paul Ngumii <?php echo date('Y'); ?></p></center>
			</footer>

		</div> <!-- /container -->

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

		<!-- Fittext.js -->
		<script src="js/jquery.fittext.js"></script> 
		
		<!-- Main JS -->
		<script src="js/main.js"></script> 
	</body>
</html>
