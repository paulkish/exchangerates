<div class="row">
	<div class="col-md-12">
		<h3>Usage</h3>
		<h4>Authentication</h4>
		<p>
			To make use of the exchangerates.co.ke API you need to get an API key. 
			<a href="signup">Sign up for one for free</a>. After confirming your email address your 
			API key will be sent directly to your email address. Always append your key to 
			your requests. E.g <code>https://exchangerates.co.ke/api/latest/key/uZUiIfbAJXe6hcI4lBMz</code>
		</p>

		<h4>Rate Limiting</h4>
		<p>
			API access is rate limited, by default each API key gets 1000 requests a month. Incase you 
			need more calls per month please feel free to <a href="contact">contact me.</a>
		<p>

		<h4>Daily Rates</h4>
		<p>
			To get the lastest or daily exchange rate use <code>https://exchangerates.co.ke/api/latest</code>
			<a class="btn btn-default daily-toggle">Show Sample Response</a>
		</p>
		<div class="daily-response"></div>

		<h4>Historical Rates</h4>
		<p>
			To get historical rates for a specific date use <code>https://exchangerates.co.ke/api/2005-01-03</code>
			<a class="btn btn-default historical-toggle">Show Sample Response</a>
		</p>
		<div class="historical-response"></div>

		<h4>Errors</h4>
		<p>The API returns errors in JSON format. Always check the <code>error</code> value as well as the 
			http <code>status</code> code. See the below error responses</p>
		<b>No Data Available</b>
<pre>
<code>{
    "error": true,
    "msg": "USER_ERROR: Incorrect API key! Ensure you are using a registered valid API key",
    "status": 500
}</code>
</pre>
<b>Rate Limit Reached</b>
<pre>
<code>{
    "error": true,
    "msg": "USER_ERROR: You have exhausted your key's number of requests for this month",
    "status": 500
}</code>
</pre>
	</div>
</div>