<div class="jumbotron">
	<h1 class="brand">exchangerates.co.ke</h1>
	<p class="lead">A Free JSON API for foreign exchange rates dating back to 2005, sourced from the 
		<a href="https://www.centralbank.go.ke/rates/forex-exchange-rates/" target="_blank">
		Central Bank of Kenya</a></p>
	<p><a class="btn btn-lg btn-success" href="signup" role="button">Get Started</a></p>
</div>