<div class="row">
	<div class="col-md-12">
		<h3>Contact</h3>
		<address>
		  	<strong>Paul Ngumii</strong><br>
		  	Email:	<?php echo hide_email('paul@ngumii.com'); ?><br>
		  	Twitter: <a href="https://twitter.com/paulngumii">@paulngumii</a>
		</address>
	</div>
</div>