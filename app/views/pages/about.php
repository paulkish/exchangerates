<div class="row">
	<div class="col-md-12">
		<h3>About</h3>
		<p>This website sources its data from the <a href="https://www.centralbank.go.ke/rates/forex-exchange-rates/" target="_blank">
		Central Bank of Kenya</a>. Exchange rates are updated daily and date back to the year 2005. At the moment 
		buying and selling rates for over 20 currencies are supported. The base currency used is KES (Kenya Shillings).
		</p>
		<p>API Uptime since 23 January 2015 is <a target="_blank" href="http://api.uptimerobot.com/getMonitors?apiKey=m776597937-1c02a701cd5af576cf9dc601&format=json">
		<?php echo $uptime->alltimeuptimeratio; ?>%</a>
		</p>
		<p>This website is maintained by <a href="https://ngumii.com">Paul Ngumii</a></p>
		<div class="alert alert-info" role="alert">
			The Central Bank of Kenya only collects exchange rates during working weekdays. 
			As such foreign exchange rates for weekends and public holidays does not exist. Rates for the current day are usually available from 10am latest.
		</div>
	</div>
</div>