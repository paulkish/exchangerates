<div class="row">
	<div class="col-md-12">
		<h3>Get Started</h3>
		<?php if($flash['error'] != NULL): ?>
			<div class="alert alert-danger" role="alert"><?php echo $flash['error']; ?></div>
		<?php endif; ?>
		<?php if($flash['success'] != NULL): ?>
			<div class="alert alert-success" role="alert"><?php echo $flash['success']; ?></div>
		<?php endif; ?>
		<p>Receive your API key via email.</p>
		<form class="form-horizontal" action="register" method="POST">
			<div class="form-group">
			    <label for="email" class="col-sm-2 control-label">Email</label>
			    <div class="col-sm-10">
				    <input type="email" class="form-control" name="email" id="email" placeholder="Enter email">
				</div>
			</div>
			<div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			    	<button type="submit" class="btn btn-primary">Send</button>
			    </div>
		  	</div>
		</form>
	</div>
</div>