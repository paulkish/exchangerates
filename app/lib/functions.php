<?php

/**                                        
 * Simple function to make use of Curl     
 * @param string $url                      
 * @return string                          
 */                                        
function curl($url)
{
    // Assigning cURL options to an array
    $options = Array(
        CURLOPT_RETURNTRANSFER => true,  // Setting cURL's option to return the webpage data
        CURLOPT_FOLLOWLOCATION => true,  // Setting cURL to follow 'location' HTTP headers
        CURLOPT_AUTOREFERER => true, // Automatically set the referer where following 'location' HTTP headers
        CURLOPT_CONNECTTIMEOUT => 120,   // Setting the amount of time (in seconds) before the request times out
        CURLOPT_TIMEOUT => 120,  // Setting the maximum amount of time for cURL to execute queries
        CURLOPT_MAXREDIRS => 10, // Setting the maximum number of redirections to follow
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; rv:34.0) Gecko/20100101 Firefox/34.0",  // Setting the useragent
        CURLOPT_URL => $url, // Setting cURL's URL option with the $url variable passed into the function
    );

    $ch = curl_init();  // Initialising cURL
    curl_setopt_array($ch, $options);   // Setting cURL's options using the previously assigned array data in $options
    $data = curl_exec($ch); // Executing the cURL request and assigning the returned data to the $data variable
    curl_close($ch);    // Closing cURL
    return $data;   // Returning the data from the function
}

/**
 * Slim middleware that add JSON response functionality to a route
 * @return false
 */
function APIrequest()
{
    $app = \Slim\Slim::getInstance();
    $app->view(new \JsonApiView()); // JSON API view response
    $app->add(new \JsonApiMiddleware()); // JSON Middleware
}

/**
 * Slim middleware that adds a html template response to a route
 * @return type
 */
function Site()
{
    $app = \Slim\Slim::getInstance();
    $app->view('\Slim\LayoutView'); // Site appearance
}


/**
 * Validate date
 * @param string $date 
 * @param string $format 
 * @return string
 */
function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

/**
 * Authenticate an API request
 * @param string $key 
 * @return mixed
 */
function authenticate($key) 
{
    $key = R::findOne('key', 'api_key = :key', [':key' =>$key]);
    if ($key != null) {
        if ($key->ignore_limits == 0) {
            return limit($key->api_key, $key->level); 
        } else {
            return true; 
        }
    } else {
        throw new Exception(
            'Incorrect API key! Ensure you are using a registered valid API key', E_USER_ERROR
        ); 
    }
}

/**
 * Checks if an API key has reached the rate limit
 * @param string $key 
 * @param int    $level 
 * @return string
 */
function limit($key,$level) 
{
    $level = R::load('level', $level); // Get no of calls allowed based on level

    //count all queries for this month to see if it has exceeded the limit
    $count = R::getCell(
        '
                SELECT count(id) as count FROM log WHERE api_key =:api 
                AND MONTH(CURDATE())=MONTH(create_date)', [':api'=>$key]
    );

    if ($count != null) {
        if ($count >= $level->calls) {
            throw new Exception("You have exhausted your key's number of requests for this month", E_USER_ERROR); 
        } else {
            return true; 
        }
    } else {
        return true; 
    }
}

/**
 * Log to database all API requests
 * @param string $authorized 
 * @param string $key 
 * @return false
 */
function log2db($authorized,$key)
{
    $app = \Slim\Slim::getInstance();
    $req = $app->request;
    $log = R::dispense('log');
    $log->uri = $req->getPath();
    $log->method = 'GET';
    $log->api_key = $key;
    $log->ip_address = $req->getIp();
    $log->user_agent = $req->getUserAgent();
    $log->create_date = date('Y-m-d H:i:s');
    $log->authorized = $authorized;
    R::store($log);
}

/**
 * Email sending function using PHPMailer
 * @param string $email 
 * @param string $subject 
 * @param string $msg 
 * @return string
 */
function email($email,$subject,$msg)
{
    global $config;
        
    //Create a new PHPMailer instance
    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->isHTML(true);  
    $mail->Host = $config->smtp_server;
    $mail->Port = $config->smtp_port;
    $mail->SMTPAuth = true;
    $mail->Username = $config->smtp_username;
    $mail->Password = $config->smtp_password;
    $mail->setFrom('no-reply@exchangerates.co.ke', 'exchangerates.co.ke');
    $mail->addReplyTo('support@exchangerates.co.ke', 'Support');
    $mail->addAddress($email);
    $mail->Subject = $subject;
    $mail->Body    = $msg;
    $mail->AltBody =  strip_tags($msg);
        
    //send the message, check for errors
    if (!$mail->send()) {
        //return $mail->ErrorInfo; 
        return false;
    } else {
        return true; 
    }
}


/**
 * Generate random key Source https://gist.github.com/raveren/5555297
 * @param type $type 
 * @param type $length 
 * @return type
 */
function random_text($type = 'alnum', $length = 8)
{
    switch ( $type ) {
    case 'alnum':
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        break;
    case 'alpha':
        $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        break;
    case 'hexdec':
        $pool = '0123456789abcdef';
        break;
    case 'numeric':
        $pool = '0123456789';
        break;
    case 'nozero':
        $pool = '123456789';
        break;
    case 'distinct':
        $pool = '2345679ACDEFHJKLMNPRSTUVWXYZ';
        break;
    default:
        $pool = (string) $type;
        break;
    }


    $crypto_rand_secure = function ( $min, $max ) {
        $range = $max - $min;
        if ($range < 0 ) { 
            return $min; // not so random...
        }        
        $log    = log($range, 2);
        $bytes  = (int) ( $log / 8 ) + 1; // length in bytes
        $bits   = (int) $log + 1; // length in bits
        $filter = (int) ( 1 << $bits ) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ( $rnd >= $range );
        return $min + $rnd;
    };

    $token = "";
    $max   = strlen($pool);
    for ( $i = 0; $i < $length; $i++ ) {
        $token .= $pool[$crypto_rand_secure( 0, $max )];
    }
    return $token;
}

/**
 * Validate Email
 * @param string $email 
 * @return string
 */
function validate_email($email)
{
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return true; 
    } else {
        return false; 
    }
}

/**
 * Check if email address exists in the database
 * @param string $email 
 * @return string
 */
function check_exist_email($email)
{
    $key = R::findOne('user', 'email = :email', [':email' =>$email]);
    if ($key != null) {
        return true; 
    } else {
        return false; 
    }
}

/**
 * Check if exchange rate exists in the database
 * @param string $date 
 * @param int    $currency 
 * @return string
 */
function check_exists_data($date,$currency)
{
    $key = R::findOne('rate', 'date = :date AND currency_id=:id', [':date' =>$date,':id'=>$currency]);
    if ($key != null) {
        return true; 
    } else {
        return false; 
    }
}

/**
 * Hide email address using JavaScript
 * @param string $email 
 * @return string
 */
function hide_email($email) 
{ 
    $character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz'; 
    $key = str_shuffle($character_set); $cipher_text = ''; $id = 'e'.rand(1, 999999999); 
    for ($i=0;$i<strlen($email);$i+=1) { 
        $cipher_text.= $key[strpos($character_set, $email[$i])]; 
    } 
    $script = 'var a="'.$key.'";var b=a.split("").sort().join("");var c="'.$cipher_text.'";var d="";'; 
    $script.= 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));'; 
    $script.= 'document.getElementById("'.$id.'").innerHTML="<a href=\\"mailto:"+d+"\\">"+d+"</a>"'; 
    $script = "eval(\"".str_replace(array("\\",'"'), array("\\\\",'\"'), $script)."\")"; 
    $script = '<script type="text/javascript">/*<![CDATA[*/'.$script.'/*]]>*/</script>'; 
    return '<span id="'.$id.'">[javascript protected email address]</span>'.$script; 
}

/**
 * Get exchange rates for a specific date
 * @param string $date 
 * @return mixed
 */
function get_rates($date)
{
    // Issue with (UGX,TSH, BIF, RWF) need to be divided by 1 and JPY divided by 100
    $rates = R::getAll(
        '
            SELECT currency.code as currency,
            IF(currency.type=:ea,ROUND(1/buying,5),IF(currency.type=:jp,buying/100,buying)) as buying,
            IF(currency.type=:ea,ROUND(1/selling,5),IF(currency.type=:jp,selling/100,selling)) as selling,
            rate.date 
            FROM rate,currency 
            WHERE date = :date AND currency.id = rate.currency_id',
        [':date' =>$date,':ea'=>2,':jp'=>3]
    );

    if ($rates != null) {
        $json = array(
            'base'=>'KES',
            'rates'=>$rates
        );

        return $json;
    } else {
        return null; 
    }
}

/**
 * Get exchange rates for a particular date range
 * @param string $start 
 * @param string $end 
 * @return mixed
 */
function get_rates_between($start,$end)
{
    // Return latest data based on date
    $rates = R::getAll(
        '
            SELECT currency.code as currency,
            IF(currency.type=:ea,ROUND(1/buying,5),IF(currency.type=:jp,buying/100,buying)) as buying,
            IF(currency.type=:ea,ROUND(1/selling,5),IF(currency.type=:jp,selling/100,selling)) as selling,
            rate.date 
            FROM rate,currency 
            WHERE date BETWEEN :start AND :end AND currency.id = rate.currency_id',
        [':start' =>$start,':end'=>$end,':ea'=>2,':jp'=>3]
    );

    if ($rates != null) {
        $json = array(
            'base'=>'KES',
            'rates'=>$rates
        );

        return $json;
    } else {
        return null; 
    }
}

/**
 * Uptime monitoring using uptimerobot.com
 * @return string
 */
function uptime()
{
    $url = 'http://api.uptimerobot.com/getMonitors?';
    $api_key='m776597937-1c02a701cd5af576cf9dc601';
    $format='json';
    $params = array(
        'apiKey'=>$api_key,
        'format'=>$format,
        'noJsonCallback'=>1
    );
    $response = curl($url.http_build_query($params));
    $decode = json_decode($response);
    return $decode->monitors->monitor[0]; 
}