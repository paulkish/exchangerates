<?php
require 'app/config/config.php';
require DIR_VENDOR.'autoload.php';


// Initialize Slim
$app = new \Slim\Slim(
    array(
        'debug' => $config->debug,
        'mode' => $config->mode,
        'log.enabled' => true,
        'log.level' => \Slim\Log::DEBUG,
        'log.writer' => new \Slim\Logger\DateTimeFileWriter(array('path'=>DIR_LOG)),
        'templates.path' => DIR_VIEWS,
        'layout' => 'layouts/main.php'
    )
);

// Middleware session cookies
$app->add(
    new \Slim\Middleware\SessionCookie(
        array(
        'expires' => '20 minutes',
        'path' => '/',
        'domain' => null,
        'secure' => false,
        'httponly' => false,
        'name' => 'slim_session',
        'secret' => 'KITiQ8lOVpN2DTOlsYpX8Ro15k9Xcj',
        'cipher' => MCRYPT_RIJNDAEL_256,
        'cipher_mode' => MCRYPT_MODE_CBC
        )
    )
);

// Configure ReadBean
R::setup("mysql:host=$config->server;dbname=$config->database", $config->username, $config->password);
R::freeze(true);

// Auth
$dbh = new PDO("mysql:host=$config->server;dbname=$config->database", $config->username, $config->password);
$cfg = new Config($dbh);
$auth = new Auth($dbh, $cfg);

// Require routes
require DIR_ROUTES.'api.php';
require DIR_ROUTES.'site.php';
require DIR_ROUTES.'cron.php';

// App run
$app->run();