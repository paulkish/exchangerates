$(function () {
    var path = 'http://'+document.location.hostname+'/rates';
    //get latest rates
    var key = 'PE8CyDplk7MaQteLP5gF';
    var date = '2005-01-03';

    // daily data get
    $('.daily-toggle').click(function() {
        $.getJSON('api/latest/key/'+key, {
            }, function (response) {
            var html = '<pre><code>';
            html += JSON.stringify(response,null,4);
            html += '</code></pre>';
            $('.daily-response').html(html);
        });
    });

    // historical data 
    $('.historical-toggle').click(function() {
        $.getJSON('api/'+date+'/key/'+key, {
            }, function (response) {
            var html = '<pre><code>';
            html += JSON.stringify(response,null,4);
            html += '</code></pre>';
            $('.historical-response').html(html);
        });
    });

    //active url highlighting
    var url = window.location.pathname,
    urlRegExp = new RegExp(url.replace(/\/$/,'') + "$");
    $('.nav a').each(function(){
        if(urlRegExp.test(this.href.replace(/\/$/,'')) && url !== '/'){ //prevent home page activating all links
            $(this).parent('li').addClass('active');
        }
    });

    $(".brand").fitText(1, { minFontSize: '18px', maxFontSize: '59px' });

    $(".brand-header").fitText(1, { minFontSize: '12px', maxFontSize: '30px' }); 
});